# set base image (host OS)
FROM python:3.10

# copy the dependencies file to the working directory
COPY ./News/requirements.txt .

# install dependencies
RUN pip install -r requirements.txt

# Install latex
RUN apt update && apt upgrade -y
RUN apt install -y latexmk texlive-latex-extra texlive-fonts-recommended

# Install rmapi
RUN cd /tmp && wget https://github.com/juruen/rmapi/releases/download/v0.0.21/rmapi-linuxx86-64.tar.gz
RUN cd /tmp && tar -xvf rmapi-linuxx86-64.tar.gz
RUN mkdir -p /usr/share/bin
RUN cd /tmp && mv rmapi /usr/share/bin/

# copy the content of the local src directory to the working directory
COPY News/ .

# command to run on container start
CMD [ "sh", "./run.sh" ]