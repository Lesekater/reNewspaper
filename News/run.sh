echo ""
echo "▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄"
echo "█ ▄▄▀█ ▄▄██ ▀██ █ ▄▄█ ███ █ ▄▄█▀▄▄▀█ ▄▄▀█▀▄▄▀█ ▄▄█ ▄▄▀█"
echo "█ ▀▀▄█ ▄▄██ █ █ █ ▄▄█▄▀ ▀▄█▄▄▀█ ▀▀ █ ▀▀ █ ▀▀ █ ▄▄█ ▀▀▄█"
echo "█▄█▄▄█▄▄▄██ ██▄ █▄▄▄██▄█▄██▄▄▄█ ████▄██▄█ ████▄▄▄█▄█▄▄█"
echo "▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀"
echo ""

# Setup rmapi config
mkdir -p $HOME/.config/rmapi/
touch $HOME/.config/rmapi/rmapi.conf
set -o noclobber
echo -n "devicetoken: " >> $HOME/.config/rmapi/rmapi.conf
echo -n "$devicetoken" >> $HOME/.config/rmapi/rmapi.conf
echo -n "\nusertoken: " >> $HOME/.config/rmapi/rmapi.conf
echo -n "$usertoken" >> $HOME/.config/rmapi/rmapi.conf

echo "Starting to generate newspaper..."
echo "Fetching articles from website..."
python3 news.py
echo "TeX file generated, trying to generate pdf file now..."

i=0
while [ $i -ne 5 ]
do
	rm log_file.txt
	latexmk -f -pdf -silent News.tex > log_file.txt
	wait

	if grep -q "error" log_file.txt
	then
		if [ $i -eq 5 ]
		then
			echo ""
			echo "An ERROR occured while generating the pdf. It is stored in 'log_file.txt':"
			cat log_file.txt
		else
			echo -en "Attempt $i of 5 failed, trying again...\r"
		fi
	else
		echo "pdf generated. Renaming..."
		mv News.pdf Newspaper.pdf
		rm News.*
		mv Newspaper.pdf News.pdf
		echo "Done                                  "
		echo "Uploading to reMarkable cloud now..."

		/usr/share/bin/rmapi rm News
		wait
		/usr/share/bin/rmapi put News.pdf
	
		echo "Done"
		echo "Enjoy your day!"
		exit
	fi
	i=$(($i+1)) #increasing iterator
done
